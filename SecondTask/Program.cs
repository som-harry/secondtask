﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecondTask.Services;

namespace SecondTask
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> primaryColor = new List<string>();
            primaryColor.Add("red");
            primaryColor.Add("blue");
            primaryColor.Add("green");
            primaryColor.Add("yellow");
            primaryColor.Add("indigo");
            primaryColor.Add("voilet");
            primaryColor.Add("orange");

            ForLoopClass forloop = new ForLoopClass();
            forloop.OutputItems(primaryColor);
            Console.ReadLine();

            SwitchCaseClass myswitch = new SwitchCaseClass();
            myswitch.OutPutItems(primaryColor);
            Console.ReadLine();

            ForEachClass forclass = new ForEachClass();
            forclass.OutPutItems(primaryColor);
            Console.ReadLine();

            IfElseClass ifclass = new IfElseClass();
            ifclass.OutPutItems(primaryColor);
            Console.ReadLine();

            WhileClass whileclass = new WhileClass();
            whileclass.OutPutItems(primaryColor);
            Console.ReadLine();

            DoWhileClass doclass = new DoWhileClass();
            doclass.OutPutItems(primaryColor);
            Console.ReadLine();

        }
    }
}
