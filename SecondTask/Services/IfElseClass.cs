﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask.Services
{
    class IfElseClass
    {
        public void OutPutItems(List<string> List)
        {
            int i = 1;
            while(i < List.Count)
            {
                if(i == 0 || i == 2)
                {
                    Console.WriteLine($"Is {List[i]} your favourite color?");
                    break;
                }
                else if(i == 1 || i == 3)
                {
                    Console.WriteLine($"Is {List[i]} color blocking");
                    break;
                }
                else
                {
                    Console.WriteLine($"Is {List[i]} your favourite color?");
                }
                i++;
            }
        }
    }
}
