﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask.Services
{
    class DoWhileClass
    {
        public void OutPutItems(List<string> List)
        {
            int i = 0;
            do
            {
                Console.WriteLine($"{List[i]} a rainbow color");
                i++;
            }
            while (i < List.Count);
           
        }
    }
}

