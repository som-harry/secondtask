﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask.Services
{
    class ForLoopClass
    {
        public void OutputItems(List<string> List)
        {
            try
            {
                for (int i = 1; i < List.Count; i++)
                {
                    Console.WriteLine($"{List[i]} is one of the primary color of rainbow");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            
        }
    }
}
