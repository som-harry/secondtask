﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask.Services
{
    class ForEachClass
    {
        public void OutPutItems(List<string> List)
        {
            foreach(string color in List)
            {
                Console.WriteLine($"{color} is among the rainbow colors");
            }
        }
    }
}
