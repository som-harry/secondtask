﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask.Services
{
    class WhileClass
    {
    public void OutPutItems(List<string> List)
        {
            try
            {
                int i = 0;
                while (i < List.Count)
                {
                    Console.WriteLine($"{List[i]} do you know while this color is important?");
                    i++;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }
    }
}
